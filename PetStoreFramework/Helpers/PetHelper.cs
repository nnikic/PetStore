﻿using System;
using System.Web.Script.Serialization;
using NUnit.Framework;
using PetStoreFramework.Data;
using RestSharp;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PetStoreFramework.Helpers
{
	public class PetHelper
	{

		public Pet createPetObject(Table table)
		{
			var photoData = table.CreateInstance<PhotoUrls>();
			var tagsData = new Tags();
			var categoryData = new Category();

			tagsData.id = Int32.Parse(table.Rows[0]["TagId"]);
			tagsData.name = table.Rows[0]["TagName"];
			categoryData.id = Int32.Parse(table.Rows[0]["CategoryId"]);
			categoryData.name = table.Rows[0]["CategoryName"];

			var petData = table.CreateInstance<Pet>();
			petData.category = categoryData;
			petData.photoUrls.Add(photoData.photoUrl);
			petData.tags.Add(tagsData);

			return petData;
		}

		public void validatePetAttribute(string attributeName, string attributeValue, Pet petResponse)
		{

			switch (attributeName)
			{
				case "Id":
					Assert.AreEqual(attributeValue, petResponse.id.ToString());
					break;
				case "CategoryId":
					Assert.AreEqual(attributeValue, petResponse.category.id.ToString());
					break;
				case "CategoryName":
					Assert.AreEqual(attributeValue, petResponse.category.name.ToString());
					break;
				case "Name":
					Assert.AreEqual(attributeValue, petResponse.name.ToString());
					break;
				case "Status":
					Assert.AreEqual(attributeValue, petResponse.status.ToString());
					break;
				case "TagId":
					Assert.AreEqual(attributeValue, petResponse.tags[0].id.ToString());
					break;
				case "TagName":
					Assert.AreEqual(attributeValue, petResponse.tags[0].name.ToString());
					break;
				case "PhotoURL":
					Assert.AreEqual(attributeValue, petResponse.photoUrls[0].ToString());
					break;
				default:
					throw new System.ArgumentException("Parameter not defined : ", attributeName);
			}

		}

		public void setupRequest(IRestRequest request, Table table)
		{
			var jsonRequest = new JavaScriptSerializer().Serialize(createPetObject(table));

			request.AddHeader("Content-Type", "application/json");
			request.AddHeader("Accept", "application/json");
			request.AddJsonBody(jsonRequest);
		}

	}
}
