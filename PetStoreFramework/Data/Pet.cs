﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PetStoreFramework.Data
{
	public class Pet
	{
		public Pet()
		{
			this.photoUrls = new List<string>();
			this.tags = new List<Tags>();
			this.category = new Category();
		}

		[JsonProperty("id")]
		public int id { get; set; }

		[JsonProperty("category")]
		public Category category { get; set; }

		[JsonProperty("name")]
		public string name { get; set; }

		[JsonProperty("photoUrls")]
		public List<string> photoUrls { get; set; }

		[JsonProperty("tags")]
		public List<Tags> tags { get; set; }

		[JsonProperty("status")]
		public string status { get; set; }
	}
}
