﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PetStoreFramework.Data
{
	public class Tags
	{
		[JsonProperty("id")]
		public int id { get; set; }
		[JsonProperty("name")]
		public string name { get; set; }
	}
}
