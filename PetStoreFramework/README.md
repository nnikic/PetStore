﻿# Techincal Test - Pet Store 
This project was developed by Nikola Nikic as Technical Test for Ding

## Prerequisites

```
| Visual Studio 2019 |
RestSharp |
NUnit |
Newtonsoft.Json |
```

## Running the tests

After building the project, you can open Test explorer where you should be able run PetStore scenario:

### Test Data
Test Data is placed under PetStore.feature, in Example Table
After every change of Test Data it is necessary to build the project again