﻿using System;
using NUnit.Framework;
using PetStoreFramework.Data;
using PetStoreFramework.Helpers;
using RestSharp;
using RestSharp.Serialization.Json;
using TechTalk.SpecFlow;

namespace PetStoreFramework.StepsDefinitions
{
	[Binding]
	public sealed class PetStoreSteps
	{
		private readonly ScenarioContext context;

		public PetStoreSteps(ScenarioContext injectedContext)
		{
			context = injectedContext;
		}

		IRestClient restClient = new RestClient().AddDefaultHeader("Cache-Control", "no-cache");
		IRestRequest request = new RestRequest()
		{
			Resource = "https://petstore.swagger.io/v2/pet"
		};
		IRestResponse response;
		PetHelper petHelper = new PetHelper();

		[Given(@"I have Pet with following details")]
		public void GivenIHavePetWithFollowingDetails(Table table)
		{
			petHelper.setupRequest(request, table);
		}

		[When(@"I Add this Pet to the Store")]
		public void WhenIAddThisPetToTheStore()
		{
			response = restClient.Post(request);
			Assert.AreEqual(200, (int)response.StatusCode);
			Console.WriteLine("### ADD RESPONSE IS : ###\n" + response.Content);
		}

		[When(@"I send request to update Pet with following details")]
		public void WhenISendRequestToUpdatePetWithFollowingDetails(Table updateTable)
		{
			IRestRequest requestUpdate = new RestRequest()
			{
				Resource = "https://petstore.swagger.io/v2/pet"
			};

			petHelper.setupRequest(requestUpdate, updateTable);
			response = restClient.Put(requestUpdate);

			Assert.AreEqual(200, (int)response.StatusCode);
			Console.WriteLine("### UPDATE RESPONSE IS : ###\n" + response.Content);
		}

		[When(@"Pet has (.*) : (.*)")]
		public void WhenPetHasId(string attributeName, string attributeValue)
		{
			Pet petResponse = new JsonDeserializer().Deserialize<Pet>(response);
			petHelper.validatePetAttribute(attributeName, attributeValue, petResponse);
		}

		[When(@"I send request to delete pet with Id : (.*)")]
		public void WhenISendRequestToDeletePetWithId(string id)
		{
			IRestRequest requestDelete = new RestRequest()
			{
				Resource = "https://petstore.swagger.io/v2/pet/" + id
			};

			response = restClient.Delete(requestDelete);
			Console.WriteLine("### DELETE RESPONSE IS : ###\n" + response.Content);
			Assert.AreEqual(200, (int)response.StatusCode);
		}

		[Then(@"I should be able to see that pet with Id : (.*) is deleted")]
		public void ThenIShouldBeAbleToSeeThatPetWithIdIsDeleted(string id)
		{
			IRestRequest requestGet = new RestRequest()
			{
				Resource = "https://petstore.swagger.io/v2/pet/" + id
			};

			response = restClient.Get(requestGet);
			Console.WriteLine("### GET AFTER DELETE RESPONSE IS : ###\n" + response.Content);
			Assert.AreEqual(404, (int)response.StatusCode);
		}

	}
}
