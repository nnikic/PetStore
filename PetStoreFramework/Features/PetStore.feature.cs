﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (https://www.specflow.org/).
//      SpecFlow Version:3.3.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace PetStoreFramework.Features
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.3.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("PetStore")]
    public partial class PetStoreFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = ((string[])(null));
        
#line 1 "PetStore.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "PetStore", @"	1. Create and return a new Pet with:
		a. Id
		b. Category_name
		c. Pet_name
		d. Status
		e. tagName
		f. photoUrl
	2. Verify the Pet was created with correct data.
	3. Update this Pet_name, Verify update and return record.
	4. Delete the Pet and demonstrate pet now deleted.
	https://petstore.swagger.io/v2/pet", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Add Pet to the Store")]
        [NUnit.Framework.CategoryAttribute("smoke")]
        [NUnit.Framework.TestCaseAttribute("1474093", "71", "Dog", "Rea", "available", "30", "HouseDog", "https://upload.wikimedia.org/wikipedia/en/1/12/Brian_Griffin.png", null)]
        public virtual void AddPetToTheStore(string id, string categoryId, string categoryName, string name, string status, string tagId, string tagName, string photoURL, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "smoke"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            System.Collections.Specialized.OrderedDictionary argumentsOfScenario = new System.Collections.Specialized.OrderedDictionary();
            argumentsOfScenario.Add("Id", id);
            argumentsOfScenario.Add("CategoryId", categoryId);
            argumentsOfScenario.Add("CategoryName", categoryName);
            argumentsOfScenario.Add("Name", name);
            argumentsOfScenario.Add("Status", status);
            argumentsOfScenario.Add("TagId", tagId);
            argumentsOfScenario.Add("TagName", tagName);
            argumentsOfScenario.Add("PhotoURL", photoURL);
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Add Pet to the Store", null, tagsOfScenario, argumentsOfScenario);
#line 15
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
                TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                            "Id",
                            "CategoryId",
                            "CategoryName",
                            "Name",
                            "Status",
                            "TagId",
                            "TagName",
                            "PhotoURL"});
                table1.AddRow(new string[] {
                            string.Format("{0}", id),
                            string.Format("{0}", categoryId),
                            string.Format("{0}", categoryName),
                            string.Format("{0}", name),
                            string.Format("{0}", status),
                            string.Format("{0}", tagId),
                            string.Format("{0}", tagName),
                            string.Format("{0}", photoURL)});
#line 17
 testRunner.Given("I have Pet with following details", ((string)(null)), table1, "Given ");
#line hidden
#line 20
 testRunner.When("I Add this Pet to the Store", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 22
 testRunner.And(string.Format("Pet has Id : {0}", id), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 23
 testRunner.And(string.Format("Pet has CategoryId : {0}", categoryId), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 24
 testRunner.And(string.Format("Pet has CategoryName : {0}", categoryName), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 25
 testRunner.And(string.Format("Pet has Name : {0}", name), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 26
 testRunner.And(string.Format("Pet has Status : {0}", status), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 27
 testRunner.And(string.Format("Pet has TagId : {0}", tagId), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 28
 testRunner.And(string.Format("Pet has TagName : {0}", tagName), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 29
 testRunner.And(string.Format("Pet has PhotoURL : {0}", photoURL), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
                TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                            "Id",
                            "CategoryId",
                            "CategoryName",
                            "Name",
                            "Status",
                            "TagId",
                            "TagName",
                            "PhotoURL"});
                table2.AddRow(new string[] {
                            string.Format("{0}", id),
                            string.Format("{0}", categoryId),
                            string.Format("{0}", categoryName),
                            "Christoph",
                            string.Format("{0}", status),
                            string.Format("{0}", tagId),
                            string.Format("{0}", tagName),
                            string.Format("{0}", photoURL)});
#line 31
 testRunner.When("I send request to update Pet with following details", ((string)(null)), table2, "When ");
#line hidden
#line 34
 testRunner.And("Pet has Name : Christoph", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 36
 testRunner.When(string.Format("I send request to delete pet with Id : {0}", id), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 37
 testRunner.Then(string.Format("I should be able to see that pet with Id : {0} is deleted", id), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
