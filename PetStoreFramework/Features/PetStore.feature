﻿Feature: PetStore
	1. Create and return a new Pet with:
		a. Id
		b. Category_name
		c. Pet_name
		d. Status
		e. tagName
		f. photoUrl
	2. Verify the Pet was created with correct data.
	3. Update this Pet_name, Verify update and return record.
	4. Delete the Pet and demonstrate pet now deleted.
	https://petstore.swagger.io/v2/pet

@smoke
Scenario: Add Pet to the Store
	#Pet creation
	Given I have Pet with following details
		| Id   | CategoryId   | CategoryName   | Name   | Status   | TagId   | TagName   | PhotoURL   |
		| <Id> | <CategoryId> | <CategoryName> | <Name> | <Status> | <TagId> | <TagName> | <PhotoURL> |
	When I Add this Pet to the Store
	#Pet verification
	And Pet has Id : <Id>
	And Pet has CategoryId : <CategoryId>
	And Pet has CategoryName : <CategoryName>
	And Pet has Name : <Name>
	And Pet has Status : <Status>
	And Pet has TagId : <TagId>
	And Pet has TagName : <TagName>
	And Pet has PhotoURL : <PhotoURL>
	#Pet update + Name verification
	When I send request to update Pet with following details
		| Id   | CategoryId   | CategoryName   | Name      | Status   | TagId   | TagName   | PhotoURL   |
		| <Id> | <CategoryId> | <CategoryName> | Christoph | <Status> | <TagId> | <TagName> | <PhotoURL> |
	And Pet has Name : Christoph
	#Pet Deletion + verification
	When I send request to delete pet with Id : <Id>
	Then I should be able to see that pet with Id : <Id> is deleted

	Examples:
		| Id      | CategoryId | CategoryName | Name | Status    | TagId | TagName  | PhotoURL                                                         |
		| 1474093 | 71         | Dog          | Rea  | available | 30    | HouseDog | https://upload.wikimedia.org/wikipedia/en/1/12/Brian_Griffin.png |